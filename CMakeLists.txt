cmake_minimum_required(VERSION 3.10)
project(cholesky)

set(CMAKE_CXX_STANDARD 14)

include_directories(
        src/matrix-generator
        src/measurement-set
        src/cholesky
        src/lib
        src/type
        src/util
        src/util/parse-arguments
)

add_executable(
        cholesky main.cpp
        src/matrix-generator/MatrixGenerator.cpp
        src/matrix-generator/MatrixGenerator.h
        src/measurement-set/MeasurementSet.cpp
        src/measurement-set/MeasurementSet.h
        src/cholesky/cholesky.cpp
        src/cholesky/cholesky.h
        src/lib/nrutil2.cpp
        src/lib/nrutil2.h
        src/lib/uklad-rownan.cpp
        src/lib/uklad-rownan.h
        src/type/aliased-types.h
        src/util/parse-arguments/parseProgramArguments.h
        src/util/parse-arguments/parseProgramArguments.cpp
        src/util/printAdditionalInfo.h
)
