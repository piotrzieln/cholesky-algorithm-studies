#include <iostream>
#include <cstdlib>
#include <MeasurementSet.h>
#include <printAdditionalInfo.h>
#include <parseProgramArguments.h>

using namespace std;

MeasurementSet *currentSet;
RegisteredParams params;

int main(int argc, char *argv[]) {
    srand(time(nullptr));
    params = parseProgramArguments(argc, argv);
    printPrologue(params.shortLog);

    for (int i = params.lowerRange; i <= params.upperRange; i += params.step) {
        currentSet = new MeasurementSet(i, params);
        currentSet->executeCalculation();
        currentSet->getExecutionInfo();
        delete currentSet;
    }

    printEpilogue(params.shortLog);
    return 0;
}
