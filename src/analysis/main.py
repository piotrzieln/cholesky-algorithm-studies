import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import make_interp_spline, BSpline


def read_values():
    lines = None
    try:
        file = open('../../log/results.txt', 'r')
        lines = file.readlines()
        for index, line in enumerate(lines):
            lines[index] = [float(i) for i in line.split(",")]
        file.close()
    except FileNotFoundError:
        print("File not found.")
    return lines


def get_attrib(attr_num):
    return list(map(lambda x: x[attr_num], DATA))


DATA = read_values()
params = DATA.pop(0)
print(params)
domain = np.array(get_attrib(0))
xnew = np.linspace(domain.min(), domain.max(), 300)

chol_spl = make_interp_spline(domain, get_attrib(1), k=3)  # type: BSpline
cholesky_smooth = chol_spl(xnew)

ctrl_spl = make_interp_spline(domain, get_attrib(3), k=3)  # type: BSpline
control_smooth = ctrl_spl(xnew)


plt.figure()
plt.plot(xnew, cholesky_smooth, label="Metoda Cholesky'ego")
plt.plot(xnew, control_smooth, label="Metoda odwracania macierzy")
plt.xlabel("DŁUGOŚĆ BOKU MACIERZY")
plt.ylabel("CZAS [ms]")
plt.legend()
plt.show()
