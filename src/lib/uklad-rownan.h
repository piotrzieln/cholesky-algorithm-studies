//
// Created by Daniel on 19.06.2020.
//

#ifndef CHOLESKY_UKLAD_ROWNAN_H
#define CHOLESKY_UKLAD_ROWNAN_H

#include <aliased-types.h>

void wyznacz_predyktor_ls(int rzad_temp, mType **iloczyny3, mType *iloczyny3_P, mType *wsp_ls);

#endif //CHOLESKY_UKLAD_ROWNAN_H
