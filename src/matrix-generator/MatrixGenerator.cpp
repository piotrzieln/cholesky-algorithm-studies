//
// Created by Daniel on 18.06.2020.
//

#include <cstring>
#include <cstdlib>
#include "MatrixGenerator.h"

MatrixGenerator::MatrixGenerator(int size, int listLen) {
    this->size = size;
    this->listLen = listLen;
    this->allocMemory();
    this->generateRandomVectors();
    this->generateRandomMatrices();
}

MatrixGenerator::MatrixGenerator(const MatrixGenerator &source) {
    this->size = source.size;
    this->listLen = source.listLen;
    this->allocMemory();
    this->copyMatrixData(source);
}

MatrixGenerator::~MatrixGenerator() {
    for (int i = 0; i < this->listLen; i++) {
        for (int j = 0; j < this->size; j++) {
            delete[] this->matrix[i][j];
        }
        delete[] this->matrix[i];
    }
    delete[] this->matrix;
}

void MatrixGenerator::allocMemory() {
    this->matrix = new mType **[this->listLen];
    this->vector = new mType *[this->listLen];
    for (int i = 0; i < this->listLen; i++) {
        this->matrix[i] = new mType *[this->size];
        this->vector[i] = new mType [this->size];
        for (int j = 0; j < this->size; j++) {
            this->matrix[i][j] = new mType [this->size];
        }
    }
}

void MatrixGenerator::generateRandomVectors() {
    for (int i = 0; i < this->listLen; i++) {
        for (int j = 0; j < this->size; j++) {
            this->vector[i][j] = this->getRandomNumber();
        }
    }
}

void MatrixGenerator::generateRandomMatrices() {
    for (int i = 0; i < this->listLen; i++) {
        for (int j = this->size - 1; j >= 0; j--) {
            for (int k = 0; k < this->size; k++) {
                if (j >= k) {
                    this->matrix[i][j][k] = this->getRandomNumber();
                } else {
                    this->matrix[i][j][k] = this->matrix[i][k][j];
                }
            }
        }
    }
}

void MatrixGenerator::copyMatrixData(const MatrixGenerator &source) {
    for (int i = 0; i < this->listLen; i++) {
        memcpy(this->getVectorNo(i), source.getVectorNo(i), this->size * sizeof(mType));
        for (int j = 0; j < this->size; j++) {
            memcpy(this->getMatrixNo(i)[j], source.getMatrixNo(i)[j], this->size * sizeof(mType));
        }
    }
}

mType **MatrixGenerator::getMatrixNo(int no) const {
    return this->matrix[no];
}

mType *MatrixGenerator::getVectorNo(int no) const {
    return this->vector[no];
}

mType MatrixGenerator::getRandomNumber() {
    return (mType)(rand() % 100);
}
