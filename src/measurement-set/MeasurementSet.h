//
// Created by Daniel on 19.06.2020.
//

#ifndef CHOLESKY_MEASUREMENTSET_H
#define CHOLESKY_MEASUREMENTSET_H

#include <ctime>
#include <chrono>
#include <parseProgramArguments.h>
#include "MatrixGenerator.h"

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::time_point<std::chrono::high_resolution_clock> TimePoint;

class MeasurementSet {

    int matrixSize;
    int attemptNumber;
    bool shortLog;
    bool choleskyOnly;
    bool controlOnly;
    bool runBoth;
    bool calculationPerformed{};

    MatrixGenerator *choleskySet;
    TimePoint choleskyTimeStart{};
    TimePoint choleskyTimeEnd{};

    MatrixGenerator *controlSet;
    TimePoint controlTimeStart{};
    TimePoint controlTimeEnd{};

public:
    MeasurementSet(int matrixSize, const RegisteredParams &params);
    int executeCalculation();
    int calculateCholesky();
    int calculateControlGroup();
    void checkIsCalculationPerformed();
    void getExecutionInfo();
    void printFullLog(long choleskyTime, long controlTime);
    void printShortLog(long choleskyTime, long controlTime);
};

#endif //CHOLESKY_MEASUREMENTSET_H
