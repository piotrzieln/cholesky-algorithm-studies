//
// Created by Daniel on 20.06.2020.
//

#include "parseProgramArguments.h"

RegisteredParams parseProgramArguments(int argc, char **argv) {
    RegisteredParams result;
    int opt;
    while ((opt = getopt(argc, argv, "shc")) != -1) {
        switch (opt) {
            case 's':
                result.shortLog = true;
                break;
            case 'h':
                result.choleskyOnly = true;
                break;
            case 'c':
                result.controlOnly = true;
                break;
            default:
                throw runtime_error("Command line params could not be parsed");
        }
    }
    if (argc > MAX_ARG_NUMBER || argc < MAX_ARG_NUMBER - 3) {
        throw runtime_error("Wrong number of arguments");
    }
    result.lowerRange = atoi(argv[argc - 4]);
    result.upperRange = atoi(argv[argc - 3]);
    result.step = atoi(argv[argc - 2]);
    result.attemptNumber = atoi(argv[argc - 1]);
    return result;
}
